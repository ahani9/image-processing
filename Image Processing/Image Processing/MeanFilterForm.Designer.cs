﻿namespace Image_Processing
{
    partial class MeanFilterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.maskHeightTextBox = new System.Windows.Forms.TextBox();
            this.maskWidthTextBox = new System.Windows.Forms.TextBox();
            this.originalHeighTextBox = new System.Windows.Forms.TextBox();
            this.originalWidthTextBox = new System.Windows.Forms.TextBox();
            this.okBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mask Height";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Original Width";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Original Height";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Mask Width";
            // 
            // maskHeightTextBox
            // 
            this.maskHeightTextBox.Location = new System.Drawing.Point(113, 21);
            this.maskHeightTextBox.Name = "maskHeightTextBox";
            this.maskHeightTextBox.Size = new System.Drawing.Size(100, 20);
            this.maskHeightTextBox.TabIndex = 4;
            // 
            // maskWidthTextBox
            // 
            this.maskWidthTextBox.Location = new System.Drawing.Point(113, 48);
            this.maskWidthTextBox.Name = "maskWidthTextBox";
            this.maskWidthTextBox.Size = new System.Drawing.Size(100, 20);
            this.maskWidthTextBox.TabIndex = 5;
            // 
            // originalHeighTextBox
            // 
            this.originalHeighTextBox.Location = new System.Drawing.Point(113, 74);
            this.originalHeighTextBox.Name = "originalHeighTextBox";
            this.originalHeighTextBox.Size = new System.Drawing.Size(100, 20);
            this.originalHeighTextBox.TabIndex = 6;
            // 
            // originalWidthTextBox
            // 
            this.originalWidthTextBox.Location = new System.Drawing.Point(113, 100);
            this.originalWidthTextBox.Name = "originalWidthTextBox";
            this.originalWidthTextBox.Size = new System.Drawing.Size(100, 20);
            this.originalWidthTextBox.TabIndex = 7;
            // 
            // okBtn
            // 
            this.okBtn.Location = new System.Drawing.Point(13, 128);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 8;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(185, 128);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 9;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            // 
            // MeanFilterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(277, 163);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.originalWidthTextBox);
            this.Controls.Add(this.originalHeighTextBox);
            this.Controls.Add(this.maskWidthTextBox);
            this.Controls.Add(this.maskHeightTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "MeanFilterForm";
            this.Text = "MeanFilterForm";
            this.Load += new System.EventHandler(this.MeanFilterForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox maskHeightTextBox;
        private System.Windows.Forms.TextBox maskWidthTextBox;
        private System.Windows.Forms.TextBox originalHeighTextBox;
        private System.Windows.Forms.TextBox originalWidthTextBox;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button cancelBtn;
    }
}