﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Image_Processing.Image_Operations
{
    public class NeighborOperations
    {
        private Bitmap image;
        private double[, ,] resultedBuffer;
        private Color[,] colorBuffer;
        public Bitmap test;

        private ImageAlgebra imageAlgebra;

        public NeighborOperations(Bitmap image)
        {
            this.image = image;
            this.test = image;
            this.resultedBuffer = new double[Enum.GetNames(typeof(COLOR)).Length, image.Height, image.Width];
            this.colorBuffer = new Color[image.Height, image.Width];
            this.imageAlgebra = new ImageAlgebra();
        }

        public Bitmap GetImage
        {
            get { return image; }
        }

        public void kirschEdgeDetection(string direction, int maskSize)
        {
            double[,] kirschFilterMask = new double[maskSize, maskSize];

            for (int i = 0; i < maskSize; i++)
            {
                for (int j = 0; j < maskSize; j++)
                {
                    if (i == 1 && j == 1)
                    {
                        kirschFilterMask[i, j] = 0;
                    }

                    else
                    {
                        kirschFilterMask[i, j] = -3;
                    }
                    
                }
            }

            if (direction == "Vertical")
            {
                kirschFilterMask[0, 0] = 5;
                kirschFilterMask[1, 0] = 5;
                kirschFilterMask[2, 0] = 5;   
            }

            else if (direction == "Horizontal")
            {
                kirschFilterMask[0, 0] = 5;
                kirschFilterMask[0, 1] = 5;
                kirschFilterMask[0, 2] = 5;
            }

            else if (direction == "NorthEast") //NE
            {
                kirschFilterMask[0, 1] = 5;
                kirschFilterMask[0, 2] = 5;
                kirschFilterMask[1, 2] = 5;
            }

            else if (direction == "NorthWest") // NW
            {
                kirschFilterMask[0, 0] = 5;
                kirschFilterMask[0, 1] = 5;
                kirschFilterMask[1, 0] = 5;
            }

            this.linearFilter(kirschFilterMask, maskSize / 2, maskSize / 2, PostProcessing.Normalize);
        }
        
        public void highBoost(double k, int maskSize, double sigma)
        {
            Bitmap originalImage = new Bitmap(this.image);
            this.gaussianFilter(maskSize, sigma);

            //Bitmap maskedImage = new Bitmap(this.imageAlgebra.subtract(originalImage, this.image));
            //this.test = new Bitmap(maskedImage);
            Bitmap result = new Bitmap(this.image.Width, this.image.Height);

            for (int x = 0; x < this.image.Width; x++)
            {
                for (int y = 0; y < this.image.Height; y++)
                {
                    Color currentImageColor = originalImage.GetPixel(x, y);
                    //Color currentMaskedImageColor = maskedImage.GetPixel(x, y);
                    Color newImage = this.image.GetPixel(x, y);

                    int newR = (int)(currentImageColor.R + k * (currentImageColor.R - newImage.R));
                    int newG = (int)(currentImageColor.G + k * (currentImageColor.G - newImage.G));
                    int newB = (int)(currentImageColor.B + k * (currentImageColor.B - newImage.B));

                    // cutoff
                    newR = Math.Min(newR, 255);
                    newR = Math.Max(newR, 0);
                    newG = Math.Min(newG, 255);
                    newG = Math.Max(newG, 0);
                    newB = Math.Min(newB, 255);
                    newB = Math.Max(newB, 0);

                    result.SetPixel(x, y, Color.FromArgb(newR, newG, newB));
                }
            }

            this.image = result;
        }

        public void laplacianSharpen(int maskSize)
        {
            double[,] laplaceFilterMask = new double[maskSize, maskSize];

            for (int i = 0; i < maskSize; i++)
            {
                for (int j = 0; j < maskSize; j++)
                {
                    if (i == 1 && j == 1)
                    {
                        laplaceFilterMask[i, j] = 9;
                    }
                    else
                    {
                        laplaceFilterMask[i, j] = -1;
                    }
                }
            }

            this.linearFilter(laplaceFilterMask, maskSize / 2, maskSize / 2, PostProcessing.CutOff);
        }

        public void meanFilter(int maskHeight, int maskWidth, int originalX, int originalY)
        {
            double[,] meanFilterMask = new double[maskHeight, maskWidth];

            originalX--;
            originalY--;

            originalX = Math.Min(originalX, maskWidth - 1);
            originalY = Math.Min(originalY, maskHeight - 1);
            originalX = Math.Max(originalX, 0);
            originalY = Math.Max(originalY, 0);

            for (int i = 0; i < maskHeight; i++)
            {
                for (int j = 0; j < maskWidth; j++)
                {
                    meanFilterMask[i, j] = 1.0 / (double)(maskWidth * maskHeight); 
                }
            }

            this.linearFilter(meanFilterMask, originalX, originalY, PostProcessing.NULL);
        }

        public void gaussianFilter(int maskSize, double sigma)
        {
            double[,] gaussianFilterMask = new double[maskSize, maskSize];
            double sum = 0.0;

            for (int i = 0; i < maskSize; i++)
            {
                for (int j = 0; j < maskSize; j++)
                {
                    int currentI = i - (maskSize / 2);
                    int currentJ = j - (maskSize / 2);
                    ;                    double gaussian = Math.Exp(-(Math.Pow(currentI, 2.0) + Math.Pow(currentJ, 2.0)) / (2.0 * Math.Pow(sigma, 2.0)));
                    sum += gaussian;
                    gaussianFilterMask[i, j] = gaussian;
                }
            }

            for (int i = 0; i < maskSize; i++)
            {
                for (int j = 0; j < maskSize; j++)
                {
                    gaussianFilterMask[i, j] = gaussianFilterMask[i, j] / sum;
                }
            }

            this.linearFilter(gaussianFilterMask, maskSize / 2, maskSize / 2, PostProcessing.NULL);
        }

        public void gaussianFilter(double sigma)
        {
            int maskSize = (int)(2.0 * Math.Round(3.7 * sigma - 0.5) + 1);
            double[,] gaussianFilterMask = new double[maskSize, maskSize];

            for (int i = 0; i < maskSize; i++)
            {
                for (int j = 0; j < maskSize; j++)
                {
                    int currentI = i - (maskSize / 2);
                    int currentJ = j - (maskSize / 2);

                    double gaussian = (1.0 / (2.0 * Math.PI * sigma * sigma)) * 
                        Math.Exp(-((double)currentI * (double)currentI + (double)currentJ * (double)currentJ) / 
                        (2.0 * sigma * sigma));

                    gaussianFilterMask[i, j] = gaussian;
                }
            }

            this.linearFilter(gaussianFilterMask, maskSize / 2, maskSize / 2, PostProcessing.NULL);
        }

        public void linearFilter(double[,] filterMask, int originalX, int originalY, PostProcessing postProcessingType)
        {
            double minR = 255;
            double minG = 255;
            double minB = 255;
            double maxR = 0;
            double maxG = 0;
            double maxB = 0;

            for (int x = 0; x < this.image.Width; x++)
            {
                for (int y = 0; y < this.image.Height; y++)
                {
                    Color temp = this.image.GetPixel(x, y);
                    this.colorBuffer[y, x] = temp;
                }
            }

            int filterMaskWidth = filterMask.GetLength(0);
            int filterMaskHeight = filterMask.GetLength(1);

            for (int x = 0; x < this.image.Width - filterMaskWidth; x++)
            {
                for (int y = 0; y < this.image.Height - filterMaskHeight; y++)
                {
                    double newR = 0.0;
                    double newG = 0.0;
                    double newB = 0.0;

                    for (int i = 0; i < filterMaskWidth; i++)
                    {
                        for (int j = 0; j < filterMaskHeight; j++)
                        {
                            Color currentColor;

                            if (y + i < 0 || x + j < 0 || y + i >= this.image.Height || x + j >= this.image.Width)
                            {
                                currentColor = Color.White;
                            }

                            else
                            {
                                currentColor = colorBuffer[y + j, x + i];
                            }

                            newR += currentColor.R * filterMask[i, j];
                            newG += currentColor.G * filterMask[i, j];
                            newB += currentColor.B * filterMask[i, j];
                        }
                    }

                    maxR = Math.Max(maxR, newR);
                    maxG = Math.Max(maxG, newG);
                    maxB = Math.Max(maxB, newB);
                    minR = Math.Min(minR, newR);
                    minG = Math.Min(minG, newG);
                    minB = Math.Min(minB, newB);

                    if (postProcessingType == PostProcessing.Absolute)
                    {
                        newR = Math.Abs(newR);
                        newG = Math.Abs(newG);
                        newB = Math.Abs(newB);
                    }

                    if (postProcessingType == PostProcessing.CutOff)
                    {
                        newR = Math.Min(newR, 255);
                        newR = Math.Max(newR, 0);
                        newG = Math.Min(newG, 255);
                        newG = Math.Max(newG, 0);
                        newB = Math.Min(newB, 255);
                        newB = Math.Max(newB, 0);
                    }

                    bool check = (y + originalY < 0 || x + originalX < 0 || y + originalY >= this.image.Height
                            || x + originalX >= this.image.Width);

                    if (check) continue;

                    if (postProcessingType == PostProcessing.Normalize)
                    {
                        this.resultedBuffer[(int)COLOR.RED, y + originalY, x + originalX] = newR;
                        this.resultedBuffer[(int)COLOR.GREEN, y + originalY, x + originalX] = newG;
                        this.resultedBuffer[(int)COLOR.BLUE, y + originalY, x + originalX] = newB;
                    }

                    else
                    {
                        this.image.SetPixel(x + originalX, y + originalY, Color.FromArgb((int)newR, (int)newG, (int)newB));
                    }
                }
            }

            if (postProcessingType == PostProcessing.Normalize)
            {
                for (int x = 0; x < this.image.Width; x++)
                {
                    for (int y = 0; y < this.image.Height; y++)
                    {
                        int normalizedR = (int)((((this.resultedBuffer[(int)COLOR.RED, y, x] - minR) / (maxR - minR)) * 255.0));
                        int normalizedG = (int)((((this.resultedBuffer[(int)COLOR.GREEN, y, x] - minG) / (maxG - minG)) * 255.0));
                        int normalizedB = (int)((((this.resultedBuffer[(int)COLOR.BLUE, y, x] - minB) / (maxB - minB)) * 255.0));

                        this.image.SetPixel(x, y, Color.FromArgb(normalizedR, normalizedG, normalizedB));
                    }
                }
            }
        }

        public enum PostProcessing
        {
            CutOff,
            Normalize,
            Absolute,
            NULL
        };

        public enum COLOR
        {
            RED = 0,
            GREEN = 1,
            BLUE = 2
        };
    }
}
