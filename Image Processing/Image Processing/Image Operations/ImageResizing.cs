﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Image_Processing.Image_Operations
{
    public class ImageResizing
    {
     /*
     * - To be continued
     * - Not related to any tasks
     * 
     */
        private Bitmap userInput;

        public ImageResizing(Bitmap userInput)
        {
            this.userInput = userInput;
        }

        private Bitmap CustomeResize(Bitmap SourcePicture, int Start, int End, int width, int height)
        {
            Bitmap Result = new Bitmap(width, height);
            using (Graphics G = Graphics.FromImage(Result))
            {
                G.SmoothingMode = SmoothingMode.HighQuality;
                G.InterpolationMode = InterpolationMode.HighQualityBilinear;
                G.PixelOffsetMode = PixelOffsetMode.HighQuality;
                G.DrawImage(SourcePicture, Start, End, height, width);
            }
            return Result;
        }

        public Bitmap PicEditBox(Bitmap Picture)
        {
            int H, W;
            if (Picture.Height < Picture.Width)
            {
                W = 500;
                H = (int)(700 * ((float)Picture.Height / (float)Picture.Width));
            }
            else
            {
                H = 500;
                W = (int)(500 * ((float)Picture.Height / (float)Picture.Width));
            }
            return CustomeResize(Picture, 0, 0, W, H);
        }

        public Bitmap BilinearResize(Bitmap Picture, int Width, int Height, bool BilinearCheck)
        {
            if (BilinearCheck == true)
            {
                double YScale, XScale;
                Color Up, Down, Left, Right;
                Up = new Color();
                Down = new Color();
                Left = new Color();
                Right = new Color();
                byte RED, GREEN, BLUE;
                byte First, Second;
                Bitmap Temp = (Bitmap)Picture.Clone();
                Picture = new Bitmap(Width, Height, Temp.PixelFormat);
                double XFactor = (double)Temp.Width / (double)Width;
                double YFactor = (double)Temp.Height / (double)Height;

                for (int x = 0; x < Picture.Width; x++)
                {
                    for (int y = 0; y < Picture.Height; y++)
                    {
                        int XFloor = (int)Math.Floor(x * XFactor);
                        int XCeil = XFloor + 1;
                        int YFloor = (int)Math.Floor(y * YFactor);
                        int YCeil = YFloor + 1;

                        if (XCeil >= Picture.Width) XCeil = XFloor;
                        if (YCeil >= Picture.Height) YCeil = YFloor;

                        XScale = x * XFactor - XFloor;
                        YScale = y * YFactor - YFloor;

                        double XSubs1 = 1.0 - XScale;
                        double YSub1 = 1.0 - YScale;

                        Up = Temp.GetPixel(XCeil, YCeil);
                        Down = Temp.GetPixel(XFloor, YFloor);
                        Left = Temp.GetPixel(XFloor, YCeil);
                        Right = Temp.GetPixel(XCeil, YFloor);

                        // RED
                        First = (byte)(XSubs1 * Down.R + XScale * Right.R);
                        Second = (byte)(XSubs1 * Left.R + XScale * Up.R);
                        RED = (byte)(YSub1 * (double)(First) + YScale * (double)(Second));

                        // BLUE
                        First = (byte)(XSubs1 * Down.B + XScale * Right.B);
                        Second = (byte)(XSubs1 * Left.B + XScale * Up.B);
                        BLUE = (byte)(YSub1 * (double)(First) + YScale * (double)(Second));

                        //GREEN
                        First = (byte)(XSubs1 * Down.G + XScale * Right.G);
                        Second = (byte)(XSubs1 * Left.G + XScale * Up.G);
                        GREEN = (byte)(YSub1 * (double)(First) + YScale * (double)(Second));

                        Picture.SetPixel(x, y, Color.FromArgb(255, RED, GREEN, BLUE));
                    }
                }
                return Picture;
            }
            else
            {
                return Picture;
            }

        }
    }
}
