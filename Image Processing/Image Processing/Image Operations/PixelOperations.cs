﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Image_Processing
{
    public class PixelOperations
    {
        #region Tint
        public Bitmap Tint(Bitmap Picture, float _red, float _green, float _blue)
        {
            BitmapData sourceData = Picture.LockBits(new Rectangle(0, 0, Picture.Width, Picture.Height), ImageLockMode.ReadOnly,
               PixelFormat.Format32bppArgb);

            byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];

            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
            Picture.UnlockBits(sourceData);
            float color = 0;

            for (int p = 0; p < pixelBuffer.Length; p += 4)
            {
                color = pixelBuffer[p];
                color += pixelBuffer[p + 1];
                color += pixelBuffer[p + 2];
                // New Color = OldColor + (255 - OldColor) * ColorTintedValue

                double R = pixelBuffer[p] + (255 - pixelBuffer[p]) * _red;
                double G = pixelBuffer[p + 1] + (255 - pixelBuffer[p + 1]) * _green;
                double B = pixelBuffer[p + 2] + (255 - pixelBuffer[p + 2]) * _blue;

                //if (R < 0) R = 1;
                if (R > 255) R = 255;

                //if (G < 0) G = 1;
                if (G > 255) G = 255;

                //if (B < 0) B = 1;
                if (B > 255) B = 255;

                pixelBuffer[p] = (byte)R;
                pixelBuffer[p + 1] = (byte)G;
                pixelBuffer[p + 2] = (byte)B;
            }

            Bitmap resBitmap = new Bitmap(Picture.Width, Picture.Height);
            BitmapData resData = resBitmap.LockBits(new Rectangle(0, 0, resBitmap.Width, resBitmap.Height), ImageLockMode.WriteOnly,
                PixelFormat.Format32bppArgb);
            Marshal.Copy(pixelBuffer, 0, resData.Scan0, pixelBuffer.Length);
            resBitmap.UnlockBits(resData);

            return resBitmap;
        }
        #endregion

        // Grayscalling an image by iteratring through the array of bytes and divide each pixel colors by 3 
        #region GrayScale
        public Bitmap GrayScale(Bitmap Picture)
        {
            BitmapData sourceData = Picture.LockBits(new Rectangle(0, 0, Picture.Width, Picture.Height), ImageLockMode.ReadOnly,
               PixelFormat.Format32bppArgb);// get the bitmap data
            //Stride to get the pixels rounded up to 4 bytes (Color, RED, GREEN, BLUE)

            byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];

            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
            Picture.UnlockBits(sourceData);
            float color = 0;

            for (int p = 0; p < pixelBuffer.Length; p += 4)
            {
                color = pixelBuffer[p] * 0.3f;
                color += pixelBuffer[p + 1] * 0.3f;
                color += pixelBuffer[p + 2] * 0.3f;

                pixelBuffer[p] = (byte)color;
                pixelBuffer[p + 1] = pixelBuffer[p];
                pixelBuffer[p + 2] = pixelBuffer[p];
                pixelBuffer[p + 3] = 255;
            }

            Bitmap resBitmap = new Bitmap(Picture.Width, Picture.Height);
            BitmapData resData = resBitmap.LockBits(new Rectangle(0, 0, resBitmap.Width, resBitmap.Height), ImageLockMode.WriteOnly,
                PixelFormat.Format32bppArgb);
            Marshal.Copy(pixelBuffer, 0, resData.Scan0, pixelBuffer.Length);
            resBitmap.UnlockBits(resData);

            return resBitmap;
        }
        #endregion
        //Increasing the photo's brigtness by increasing the colors by the user input 
        #region Brightness
        public Bitmap Brightness(Bitmap Picture, int Bright)
        {
            BitmapData sourceData = Picture.LockBits(new Rectangle(0, 0, Picture.Width, Picture.Height), ImageLockMode.ReadOnly,
               PixelFormat.Format32bppArgb);

            byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];

            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
            Picture.UnlockBits(sourceData);
            float color = 0;

            for (int p = 0; p < pixelBuffer.Length; p += 4)
            {
                color = pixelBuffer[p];
                color += pixelBuffer[p + 1];
                color += pixelBuffer[p + 2];

                int R = pixelBuffer[p] + Bright;

                R = Math.Min(R, 255);
                R = Math.Max(R, 0);

                int G = pixelBuffer[p + 1] + Bright;

                G = Math.Min(G, 255);
                G = Math.Max(G, 0);

                int B = pixelBuffer[p + 2] + Bright;

                B = Math.Min(B, 255);
                B = Math.Max(B, 0);

                pixelBuffer[p] = (byte)R;
                pixelBuffer[p + 1] = (byte)G;
                pixelBuffer[p + 2] = (byte)B;
            }

            Bitmap resBitmap = new Bitmap(Picture.Width, Picture.Height);
            BitmapData resData = resBitmap.LockBits(new Rectangle(0, 0, resBitmap.Width, resBitmap.Height), ImageLockMode.WriteOnly,
                PixelFormat.Format32bppArgb);
            Marshal.Copy(pixelBuffer, 0, resData.Scan0, pixelBuffer.Length);
            resBitmap.UnlockBits(resData);

            return resBitmap;
        }
        #endregion
        // Invert the images color
        #region Invert
        public Bitmap Invert(Bitmap Picture)
        {
            BitmapData sourceData = Picture.LockBits(new Rectangle(0, 0, Picture.Width, Picture.Height), ImageLockMode.ReadOnly,
               PixelFormat.Format32bppArgb);

            byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];

            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
            Picture.UnlockBits(sourceData);
            float color = 0;

            for (int p = 0; p < pixelBuffer.Length; p += 4)
            {
                color = pixelBuffer[p];
                color += pixelBuffer[p + 1];
                color += pixelBuffer[p + 2];

                pixelBuffer[p] = (byte)(255 - pixelBuffer[p]);
                pixelBuffer[p + 1] = (byte)(255 - pixelBuffer[p + 1]);
                pixelBuffer[p + 2] = (byte)(255 - pixelBuffer[p + 2]);
            }

            Bitmap resBitmap = new Bitmap(Picture.Width, Picture.Height);
            BitmapData resData = resBitmap.LockBits(new Rectangle(0, 0, resBitmap.Width, resBitmap.Height), ImageLockMode.WriteOnly,
                PixelFormat.Format32bppArgb);
            Marshal.Copy(pixelBuffer, 0, resData.Scan0, pixelBuffer.Length);
            resBitmap.UnlockBits(resData);

            return resBitmap;

        }
        #endregion
        // Gamma Correction using a helper function to calculate the formula
        #region Gamma
        public Bitmap Gamma(Bitmap Picture, double gamaValue)
        {
            BitmapData sourceData = Picture.LockBits(new Rectangle(0, 0, Picture.Width, Picture.Height), ImageLockMode.ReadOnly,
               PixelFormat.Format32bppArgb);

            byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];

            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
            Picture.UnlockBits(sourceData);

            int colorR = 0;
            int colorG = 0;
            int colorB = 0;

            for (int p = 0; p < pixelBuffer.Length; p += 4)
            {
                colorR = pixelBuffer[p];
                colorG = pixelBuffer[p + 1];
                colorB = pixelBuffer[p + 2];

                int newR = (int)(Math.Pow((double)colorR / 256.0, 1.0 / gamaValue) * 255.0);
                int newG = (int)(Math.Pow((double)colorG / 256.0, 1.0 / gamaValue) * 255.0);
                int newB = (int)(Math.Pow((double)colorB / 256.0, 1.0 / gamaValue) * 255.0);

                newR = Math.Min(newR, 255);
                newR = Math.Max(newR, 0);

                newG = Math.Min(newG, 255);
                newG = Math.Max(newG, 0);

                newB = Math.Min(newB, 255);
                newB = Math.Max(newB, 0);

                pixelBuffer[p] = (byte)newR;
                pixelBuffer[p + 1] = (byte)newG;
                pixelBuffer[p + 2] = (byte)newB;

            }

            Bitmap resBitmap = new Bitmap(Picture.Width, Picture.Height);
            BitmapData resData = resBitmap.LockBits(new Rectangle(0, 0, resBitmap.Width, resBitmap.Height), ImageLockMode.WriteOnly,
                PixelFormat.Format32bppArgb);
            Marshal.Copy(pixelBuffer, 0, resData.Scan0, pixelBuffer.Length);
            resBitmap.UnlockBits(resData);

            return resBitmap;
        }

        private static byte[] GammaArrays(double Col)
        {
            byte[] Arr = new byte[255 + 1];

            for (int i = 0; i < 255 + 1; i++)
            {
                Arr[i] = (byte)Math.Min(255, (int)((255.0 * Math.Pow(i / 255.0, 1.0 / Col)) + 0.5));
            }
            return Arr;
        }

        #endregion

        // Contrasting photo
        #region Contrast
        public Bitmap Contrast(Bitmap Picture, int newMin, int newMax)
        {
            int minR = 255;
            int minG = 255;
            int minB = 255;
            int maxR = 0;
            int maxG = 0;
            int maxB = 0;

            BitmapData sourceData = Picture.LockBits(new Rectangle(0, 0, Picture.Width, Picture.Height), ImageLockMode.ReadOnly,
               PixelFormat.Format32bppArgb);

            byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];

            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
            Picture.UnlockBits(sourceData);
            float color = 0;

            for (int p = 0; p < pixelBuffer.Length; p += 4)
            {
                color = pixelBuffer[p];
                color += pixelBuffer[p + 1];
                color += pixelBuffer[p + 2];

                maxR = Math.Max(maxR, pixelBuffer[p]);
                minR = Math.Min(minR, pixelBuffer[p]);
                maxG = Math.Max(maxG, pixelBuffer[p + 1]);
                minG = Math.Min(minG, pixelBuffer[p + 1]);
                maxB = Math.Max(maxB, pixelBuffer[p + 2]);
                minB = Math.Min(minB, pixelBuffer[p + 2]);
            }

            for (int p = 0; p < pixelBuffer.Length; p += 4)
            {
                float currentRed = pixelBuffer[p];
                float currentGreen = pixelBuffer[p + 1];
                float currentBlue = pixelBuffer[p + 2];

                int newR = (int)((((float)(currentRed - minR) / (float)(maxR - minR)) * (float)newMax) + newMin);
                int newG = (int)((((float)(currentGreen - minG) / (float)(maxG - minG)) * (float)newMax) + newMin);
                int newB = (int)((((float)(currentBlue - minB) / (float)(maxB - minB)) * (float)newMax) + newMin);

                newR = Math.Min(newR, 255);
                newR = Math.Max(newR, 0);

                newG = Math.Min(newG, 255);
                newG = Math.Max(newG, 0);

                newB = Math.Min(newB, 255);
                newB = Math.Max(newB, 0);

                pixelBuffer[p] = (byte)newR;
                pixelBuffer[p + 1] = (byte)newG;
                pixelBuffer[p + 2] = (byte)newB;
            }

            Bitmap resBitmap = new Bitmap(Picture.Width, Picture.Height);
            BitmapData resData = resBitmap.LockBits(new Rectangle(0, 0, resBitmap.Width, resBitmap.Height), ImageLockMode.WriteOnly,
                PixelFormat.Format32bppArgb);
            Marshal.Copy(pixelBuffer, 0, resData.Scan0, pixelBuffer.Length);
            resBitmap.UnlockBits(resData);

            return resBitmap;
        }
        #endregion

        #region Bit-Slicing
        public Bitmap bitSlicing(Bitmap image, byte mask, bool grayScale = false)
        {
            BitmapData sourceData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly,
               PixelFormat.Format32bppArgb);

            byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];

            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
            image.UnlockBits(sourceData);
            float color = 0;

            if (grayScale == true)
            {
                for (int p = 0; p < pixelBuffer.Length; p += 4)
                {
                    color = pixelBuffer[p] * 0.20f;
                    color += pixelBuffer[p + 1] * 0.50f;
                    color += pixelBuffer[p + 2] * 0.3f;

                    pixelBuffer[p] = (byte)color;
                    pixelBuffer[p + 1] = pixelBuffer[p];
                    pixelBuffer[p + 2] = pixelBuffer[p];
                    pixelBuffer[p + 3] = 255;
                }
            }

            for (int p = 0; p < pixelBuffer.Length; p += 4)
            {
                pixelBuffer[p] = Convert.ToByte(pixelBuffer[p] & mask) > (byte)0 ? (byte)255 : (byte)0;
                pixelBuffer[p + 1] = Convert.ToByte(pixelBuffer[p + 1] & mask) > (byte)0 ? (byte)255 : (byte)0;
                pixelBuffer[p + 2] = Convert.ToByte(pixelBuffer[p + 2] & mask) > (byte)0 ? (byte)255 : (byte)0;
                pixelBuffer[p + 3] = 255;
            }

            Bitmap resBitmap = new Bitmap(image.Width, image.Height);
            BitmapData resData = resBitmap.LockBits(new Rectangle(0, 0, resBitmap.Width, resBitmap.Height), ImageLockMode.WriteOnly,
                PixelFormat.Format32bppArgb);
            Marshal.Copy(pixelBuffer, 0, resData.Scan0, pixelBuffer.Length);
            resBitmap.UnlockBits(resData);

            return resBitmap;
        }
        #endregion
    }
}
