﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Image_Processing.Image_Operations
{
    public class HistogramOperations
    {
        private int[] red1;
        private int[] blue1;
        private int[] green1;

        private int[] red2;
        private int[] blue2;
        private int[] green2;

        Histogram h;

        public HistogramOperations()
        {
            
        }

        public void setToBeEqualized(int[] red1, int[] green1, int[] blue1)
        {
            this.red1 = red1;
            this.green1 = green1;
            this.blue1 = blue1;
        }

        public void setToBeMatched(int[] red1, int[] green1, int[] blue1, int[] red2, int[] green2, int[] blue2)
        {
            this.red1 = red1;
            this.green1 = green1;
            this.blue1 = blue1;

            this.red2 = red2;
            this.green2 = green2;
            this.blue2 = blue2;
        }

        public Bitmap runEqualization(Bitmap image)
        {
            int[] newR = new int[256];
            int[] newG = new int[256];
            int[] newB = new int[256];

            //for (int i = 0; i < 256; i++)
            //{
            //    cumSumR += red1[i];
            //    cumSumG += green1[i];
            //    cumSumB += blue1[i];
            //}

            //int temCumSumR = 0, temCumSumG = 0, temCumSumB = 0;

            //for (int i = 0; i < 256; i++)
            //{
            //    temCumSumR += red1[i];
            //    temCumSumG += green1[i];
            //    temCumSumB += blue1[i];

            //    float temp = (float)temCumSumR / cumSumR;
            //    newR[i] = (int)(temp * 255);

            //    temp = (float)temCumSumG / cumSumG;
            //    newG[i] = (int)(temp * 255);

            //    temp = (float)temCumSumB / cumSumB;
            //    newB[i] = (int)(temp * 255);
            //}

            Equalize(ref newR, ref newG, ref newB, true);
            this.h = new Histogram();

            h.setData(newR, newG, newB);
            h.drawHistogram();
            h.Show();


            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    Color currentColor = image.GetPixel(i, j);
                    currentColor = Color.FromArgb((byte)newR[Convert.ToInt32(currentColor.R)],
                        (byte)newG[Convert.ToInt32(currentColor.G)],
                        (byte)newB[Convert.ToInt32(currentColor.B)]);

                    image.SetPixel(i, j, currentColor);
                }
            }

            return image;
        }


        private void Equalize(ref int[] newR, ref int[] newG, ref int[] newB, bool first)
        {
            int cumSumR = 0;
            int cumSumG = 0;
            int cumSumB = 0;

            //int[] newR = new int[256];
            //int[] newG = new int[256];
            //int[] newB = new int[256];

            for (int i = 0; i < 256; i++)
            {
                if (first)
                {
                    cumSumR += red1[i];
                    cumSumG += green1[i];
                    cumSumB += blue1[i];
                }
                else
                {
                    cumSumR += red2[i];
                    cumSumG += green2[i];
                    cumSumB += blue2[i];
                }
            }

            int temCumSumR = 0, temCumSumG = 0, temCumSumB = 0;

            for (int i = 0; i < 256; i++)
            {
                if (first)
                {
                    temCumSumR += red1[i];
                    temCumSumG += green1[i];
                    temCumSumB += blue1[i];
                }
                else
                {
                    temCumSumR += red2[i];
                    temCumSumG += green2[i];
                    temCumSumB += blue2[i];
                }


                float temp = (float)temCumSumR / cumSumR;
                newR[i] = (int)(temp * 255);

                temp = (float)temCumSumG / cumSumG;
                newG[i] = (int)(temp * 255);

                temp = (float)temCumSumB / cumSumB;
                newB[i] = (int)(temp * 255);
            }
        }

        private void replacedColorSearching(int[] First, int[] Second, ref int[] newColor)
        {
            for (int i = 0; i < 256; i++)
            {
                int diff, index;

                bool Backward = true, Forward = true;
                diff = Math.Abs(First[i] - Second[i]);
                index = i;
                int j = 1;
                while (i + j < 256 || i - j >= 0)
                {
                    if (i - j >= 0 && Backward)
                    {
                        int absDiffBackward = Math.Abs(First[i] - Second[i - j]);
                        if (absDiffBackward <= diff)
                        {
                            diff = absDiffBackward;
                            index = i - j;
                        }
                        else
                            Backward = false;
                    }
                    if (i + j < 256 && Forward)
                    {
                        int absDiffForward = Math.Abs(First[i] - Second[i + j]);
                        if (absDiffForward < diff)
                        {
                            diff = absDiffForward;
                            index = i + j;
                        }
                        else
                            Forward = false;
                    }

                    if (i + j >= 256 && i - j < 0 && !Forward && !Backward)
                    {
                        break;
                    }

                    j++;
                }
                newColor[i] = index;
            }
        }

        public Bitmap runMatching(Bitmap image)
        {
            // 
            int[] EquR1 = new int[256];
            int[] EquG1 = new int[256];
            int[] EquB1 = new int[256];

            int[] EquR2 = new int[256];
            int[] EquG2 = new int[256];
            int[] EquB2 = new int[256];

            Equalize(ref EquR1, ref EquG1, ref EquB1, true);
            Equalize(ref EquR2, ref EquG2, ref EquB2, false);

            int[] newR = new int[256];
            int[] newG = new int[256];
            int[] newB = new int[256];
            replacedColorSearching(EquR1, EquR2, ref newR);
            replacedColorSearching(EquG1, EquG2, ref newG);
            replacedColorSearching(EquB1, EquB2, ref newB);

            this.h = new Histogram();

            h.setData(newR, newG, newB);
            h.drawHistogram();
            h.Show();

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    Color currentColor = image.GetPixel(i, j);
                    currentColor= Color.FromArgb((byte)newR[Convert.ToInt32(currentColor.R)], 
                        (byte)newG[Convert.ToInt32(currentColor.G)], 
                        (byte)newB[Convert.ToInt32(currentColor.B)]);
                    
                    image.SetPixel(i, j, currentColor);
                }
            }

            return image;
        }

    }
}
