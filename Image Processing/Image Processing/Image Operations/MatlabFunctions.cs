﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using MATLABTASKS;
using System.Drawing;

namespace Image_Processing.Image_Operations
{
    public class MatlabFunctions
    {
        private MATLABTASKS.Class1 matlab;
        private Bitmap image;
        private double[,,] imageBuffer;

        public MatlabFunctions(Bitmap image)
        {
            this.image = image;
            this.imageBuffer = new double[image.Width, image.Height, 3];

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    Color pixel = this.image.GetPixel(i, j);
                    this.imageBuffer[i, j, 0] = pixel.R;
                    this.imageBuffer[i, j, 1] = pixel.G;
                    this.imageBuffer[i, j, 2] = pixel.B;
                }
            }
        }

        public Bitmap threshold()
        {
            this.matlab = new Class1();
            Bitmap res = new Bitmap(this.image.Width, this.image.Height);

            double[,,] resultedBuffer = (double[,,])(this.matlab.LocalHE((MWNumericArray)this.imageBuffer, 3)).ToArray();

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    double tempR = resultedBuffer[i, j, 0];
                    double tempG = resultedBuffer[i, j, 1];
                    double tempB = resultedBuffer[i, j, 2];
                    res.SetPixel(i, j, Color.FromArgb((int)tempR, (int)tempG, (int)tempB));
                }
            }

            return res;
        }
    }
}
