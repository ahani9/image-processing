﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Image_Processing
{
    public partial class KirschEdgeDetectionForm : Form
    {
        public KirschEdgeDetectionForm()
        {
            InitializeComponent();

            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("Vertical", "Vertical");
            list.Add("Horizontal", "Horizontal");
            list.Add("NorthWest", "NorthWest");
            list.Add("NorthEast", "NorthEast");

            this.directionsComboBox.DataSource = new BindingSource(list, null);
            this.directionsComboBox.DisplayMember = "Key";
            this.directionsComboBox.ValueMember = "Value";

            okBtn.DialogResult = DialogResult.OK;
            cancelBtn.DialogResult = DialogResult.Cancel;
        }

        public string GetDirection
        {
            get { return directionsComboBox.Text; }
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
