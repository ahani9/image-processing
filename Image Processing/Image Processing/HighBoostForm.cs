﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Image_Processing
{
    public partial class HighBoostForm : Form
    {
        public HighBoostForm()
        {
            InitializeComponent();

            okBtn.DialogResult = DialogResult.OK;
            cancelBtn.DialogResult = DialogResult.Cancel;
        }

        public double K
        {
            get { return Convert.ToDouble(ktextBox.Text); }
        }

        public int MaskSize
        {
            get { return Convert.ToInt32(maskSizeTextBox.Text); }
        }

        public double Sigma
        {
            get { return Convert.ToDouble(sigmaTextBox.Text); }
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
