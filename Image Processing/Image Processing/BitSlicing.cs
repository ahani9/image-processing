﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Image_Processing
{
    public partial class BitSlicing : Form
    {
        public BitSlicing()
        {
            InitializeComponent();
            OK.DialogResult = DialogResult.OK;
            Cancel.DialogResult = DialogResult.Cancel;
        }

        public byte MaskValue
        {
            get
            {

                int x = Convert.ToInt32(maskBox.Text);
                string temp = "";

                for (int i = 0; i < 8; i++)
                {
                    temp += '0';
                }

                string temp2 = "";

                for (int i = 0; i < 8; i++)
                {
                    if (i == x - 1)
                    {
                        temp2 += '1';
                    }
                    else
                    {
                        temp2 += '0';
                    }
                }
                    string tempI = Convert.ToString(temp2);
                    byte final = Convert.ToByte(tempI, 2);
                    return (final);
            }

            set { maskBox.Text = value.ToString(); }
        }

        public bool GrayScale
        {
            get { return (this.grayCheck.Checked); }
            set { this.grayCheck.Checked = value; }
        }

        private void OK_Click(object sender, EventArgs e)
        {

        }

        private void BitSlicing_Load(object sender, EventArgs e)
        {

        }
    }
}
