﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Image_Processing
{
    public partial class GaussianForm : Form
    {
        public GaussianForm(bool check)
        {
            InitializeComponent();

            if (check)
            {
                maskSizeTextBox.Visible = false;
                label2.Visible = false;
            }

            okBtn.DialogResult = DialogResult.OK;
            cancelBtn.DialogResult = DialogResult.Cancel;
        }

        public int MaskSize
        {
            get {
                if (maskSizeTextBox.Text == "") return 0;
                return Convert.ToInt32(maskSizeTextBox.Text); 
            }
        }

        public double Sigma
        {
            get { return Convert.ToDouble(sigmaTextBox.Text); }
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
