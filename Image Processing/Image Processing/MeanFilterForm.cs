﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Image_Processing
{
    public partial class MeanFilterForm : Form
    {
        public MeanFilterForm()
        {
            InitializeComponent();

            okBtn.DialogResult = DialogResult.OK;
            cancelBtn.DialogResult = DialogResult.Cancel;
        }

        public int MaskHeight
        {
            get { return Convert.ToInt32(maskHeightTextBox.Text); }
        }

        public int MaskWidth
        {
            get { return Convert.ToInt32(maskWidthTextBox.Text); }
        }

        public int OriginalHeight
        {
            get { return Convert.ToInt32(originalHeighTextBox.Text); }
        }

        public int OriginalWidth
        {
            get { return Convert.ToInt32(originalWidthTextBox.Text); }
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MeanFilterForm_Load(object sender, EventArgs e)
        {

        }
    }
}
