﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Image_Processing
{
    public partial class ContrastForm : Form
    {
        public double NewMin
        {
            get { return (Convert.ToDouble(newMinTextBox.Text)); }
        }

        public double NewMax
        {
            get { return (Convert.ToDouble(newMaxTextBox.Text)); }
        }

        public ContrastForm()
        {
            InitializeComponent();
            OK.DialogResult = DialogResult.OK;
            Cancel.DialogResult = DialogResult.Cancel;
        }

        private void ContrastForm_Load(object sender, EventArgs e)
        {

        }

        private void OK_Click(object sender, EventArgs e)
        {

        }
    }
}
